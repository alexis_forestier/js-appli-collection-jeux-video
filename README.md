# JS Video Games Collection Manager

![](/GameCollectionJS.jpg)

## Collaborators
- [Alexis FORESTIER](https://gitlab.com/alexis_forestier)
- [Erwan GERBA](https://gitlab.com/ErwanGe)

## Description
Web application that allows you to manage a collection of video games (add, read data, edit, delete)

## Requirements
- Node.js ([download](https://nodejs.org/en/download/))
- MySQL ([download](https://dev.mysql.com/downloads/installer/))

This project is built for a Windows OS

## Installation
- clone this project
- launch "install.bat" to install all dependencies
- launch "launch-server.bat" (If a database error appears, it means that you have not launched the MySQL80 service)
- launch "launch-vue.bat"
- open your browser and go to `http://127.0.0.1:3001/`

### if you don't have a windows OS
- Install dependencies

For the server
```
cd .\server\server-collection\
npm i
```
For the vue
```
cd .\vue\vue-collection\
npm i
```
- Start server
```
cd .\server\server-collection\
npm run dev
```
- Start vue
```
cd .\vue\vue-collection\
npm run dev
```

## Advancement

Theme : video-games &#x1F3AE;

### Front-end

- All video games page : &#10004;
    - paginated page : &#10004;
    - filter content by type : &#10004;
- Video game details page : &#10004;
- Add video game page : &#10004;
- Update video game page : &#10004;
- Login page : &#128679;

### Back-end

- Get all video games : &#10004;
- Get video game : &#10004;
- Add video games : &#10004;
- Update video games : &#10004;
- Delete video games : &#10004;
