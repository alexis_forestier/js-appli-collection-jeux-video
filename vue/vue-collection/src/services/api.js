import axios from 'axios'

const apiUrl = 'http://127.0.0.1:3000';

export const getVideoGames = async (page) => {
    const res = await axios.get(`${apiUrl}/videogames/?page=${page}`);
    return res.data
}

export const getVideoGame = async (id) => {
    const res = await axios.get(`${apiUrl}/videogames/${id}`);
    return res.data
}

export const getVideoGamesWithFilter = async (filter) => {
    const res = await axios.get(`${apiUrl}/videogames/filter/${filter}`);
    return res.data
}

export const addVideoGame = async (VideoGame) => {
    const res = await axios.post(`${apiUrl}/videogames/add`, VideoGame)
    return res.data
}

export const updateVideoGame = async (videoGame) => {
    console.log(videoGame)
    const res = await axios.put(`${apiUrl}/videogames/`, videoGame);
    return res.data;
}

export const deleteVideoGame = async (id) => {
    await axios.delete(`${apiUrl}/videogames/${id}`);
}

export const loginUser = async () => {
    const res = await axios.post(`${apiUrl}/users/login`);
    console.log(res.data);
    return res.data;
}