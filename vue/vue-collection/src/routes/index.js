import Home from '../page/Home.vue'
import Add from '../page/Add.vue'
import Login from '../page/Login.vue'
import View from '../page/View.vue'
import Edit from '../page/Edit.vue'
import { createRouter, createWebHistory } from 'vue-router'
const routes = [
    { path: '/', name: 'Home', component: Home },
    { path: '/add', name: 'Add', component: Add },
    { path: '/login', name: 'Login', component: Login },
    { path: '/view/:id', name: 'View', component: View },
    { path: '/edit/:id', name: 'Edit', component: Edit },
]

export const router = createRouter({
    history: createWebHistory(),
    routes
})
