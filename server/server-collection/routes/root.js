'use strict'

import VideoGame from "./videogames/index.js"
import User from "./users/index.js"

export default async function (fastify, opts) {
  fastify.get('/', async function (request, reply) {
    return { root: true }
  })
  fastify.register(VideoGame, {prefix: 'videogames'})
  fastify.register(User, {prefix: 'users'})
}
