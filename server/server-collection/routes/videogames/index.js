"use strict";

import { VideoGame } from "../../database.js";

export default async function (fastify, opts) {
  fastify.get("/", async function (request, reply) {
    if (request.query.name) {
      return await VideoGame.findAll({ where: { name: request.query.name } });
    } else if (request.query.page){
        let page = 1;
        if(request.query.page != 0) {
            page = request.query.page * 18 - 18;
        }
        return await VideoGame.findAll({limit: 18, offset: page});
    }
    else {
        return await VideoGame.findAll({limit: 18, offset: 0});
    }
  });

  fastify.get("/:id", async function (request, reply) {
    return await VideoGame.findByPk(request.params.id);
  });

  fastify.get("/filter/:filter", async function (request, reply) {
    return await VideoGame.findAll({ 
      where: { 
        type :  request.params.filter
      }
    });
  });

  fastify.post("/add", async function (request, reply) {
    console.log(request.body);
    if (!request.body?.name) {
      reply.code(400).send("T'es con ou quoi ?");
    }
    return await VideoGame.create({
      name: request.body.name,
      platform: request.body.platform,
      creator: request.body.creator,
      version: request.body.version,
      note: request.body.note,
      type: request.body.type,
      price: request.body.price,
      img: request.body.img
    });
  });

  fastify.put("/", async function (request, reply) {
    if (request.body) {
      return await VideoGame.update(request.body, {
        where: { id: request.body.id },
      });
    }
  });

  fastify.delete("/:id", async function (request, reply) {
    if (request.params?.id) {
      return await VideoGame.destroy({ where: { id: request.params.id } });
    }
  });

}
