'use strict'

import { User } from '../../database.js'

export default async function (fastify, opts) {


    fastify.post("/login", async function (request, reply) {
        if (!request.body?.email) {
            reply.code(400).send("Bad request");
        }
        else {
            let user = await User.findAll({ where: { email: request.body.email } });
            if (user.length === 0) {
                reply.code(404).send("This user doesn't exist");
            }
            else {
                if (await fastify.bcrypt.compare(request.body.password, user[0].password)) {
                    return {
                        id: user[0].id,
                        name: user[0].name,
                        email: user[0].email,
                    }
                }
                reply.code(400).send("Wrong password");
            }
        }
    })

    //fastify.bcrypt.hash('password')
    fastify.post('/signup', async function (request, reply) {

        if (!request.body?.email) {
            reply.code(400).send("T'es con ou quoi ?");
        }

        return await User.create({
            name: "user",
            email: request.body.email,
            password: await fastify.bcrypt.hash(request.body.password),
            admin: 0
        });
    })

    fastify.delete('/:id', async function (request, reply) {
        if (request.params?.id) {
            return await User.destroy(
                { where: { id: request.params.id } }
            )
        }
    })
}
