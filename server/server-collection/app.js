'use strict'

import sensible from './plugins/sensible.js';
import support from './plugins/support.js';
import routes from './routes/root.js';
import './database.js';
import fastifyCors from 'fastify-cors';
import bcrypt from 'fastify-bcrypt';
import pagination from 'fastify-pagination'
export default async function (fastify, opts) {
  fastify.register(sensible)
  fastify.register(support)
  fastify.register(fastifyCors)
  fastify.register(routes)
  fastify.register(bcrypt, {
    saltWorkFactor: 12
  })
  fastify.register(pagination)
}

