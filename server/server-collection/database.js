import { DataTypes, Sequelize } from 'sequelize';

const sequelize = new Sequelize('mysql://root:RootKey_01@127.0.0.1:3306/database_jv')
try {
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');
} catch (error) {
    console.error('Unable to connect to the database:', error);
}
 
export const VideoGame = sequelize.define('VideoGame', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    platform: {
        type: DataTypes.STRING,
        defaultValue: "Other"
    },
    creator: {
        type: DataTypes.STRING,
        defaultValue: "Anonymous"
    },
    version: {
        type: DataTypes.STRING
    },
    note: {
        type: DataTypes.INTEGER
    },
    type: {
        type: DataTypes.STRING
    },
    price: {
        type: DataTypes.DOUBLE
    },
    img: {
        type: DataTypes.STRING,
    }
});
// Création de la table si elle n'existe pas
VideoGame.sync()

export const User = sequelize.define('User', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    admin: {
        type: DataTypes.BOOLEAN,
        defaultValue: 0
    }
});
// Création de la table si elle n'existe pas
User.sync()